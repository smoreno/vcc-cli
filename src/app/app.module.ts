import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app.routing';

// Modules
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { AdministratorModule } from './web/main/administrator/administrator.module';
import { AuthenticationModule } from './web/main/authentication/authentication.module';
import { OnlineModule } from './web/main/online/online.module';

// Components
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  bootstrap: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    SharedModule,
    AdministratorModule,
    AuthenticationModule,
    OnlineModule,
    AppRoutingModule,
  ],
})
export class AppModule {}
