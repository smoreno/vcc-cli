import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';

const routes: Routes = [{
  path: '',
  redirectTo: 'auth',
  pathMatch: 'full'
}, {
  path: 'administrator',
  loadChildren: './web/main/administrator/administrator.module#AdministratorModule'
}, {
  path: 'online',
  loadChildren: './web/main/online/online.module#OnlineModule'
}, {
  path: 'auth',
  loadChildren: './web/main/authentication/authentication.module#AuthenticationModule'
}, {
  path: '**',
  component: PageNotFoundComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
