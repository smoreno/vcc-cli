import { Injectable } from '@angular/core';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';

@Injectable()
export class CustomNgbModalConfig {
  // Propiedades
  public options: any;

  /**
   * Constructor del servicio CustomNgbModalConfig
   * @param modalService Servicio de las modales
   * @param config Servicio para las configuraciones
   */
  constructor(private modalService: NgbModal, config: NgbModalConfig) {
    // Configuración general
    config.backdrop = 'static';
    config.keyboard = false;

    // Optiones para la modal
    this.options = {
      windowClass: '',
      backdropClass: '',
      ariaLabelledBy: 'modal-basic-title',
      size: 'lg',
      centered: false
    } as any;
  }

  /**
   * Abre una modal en formato pequeño
   * @param content Contenido HTML
   */
  openSm(content: any) {
    this.options.size = 'sm';
    return this.modalService.open(content, this.options);
  }

  /**
   * Abre una modal en formato grande
   * @param content Contenido HTML
   */
  openLg(content: any) {
    this.options.size = 'lg';
    return this.modalService.open(content, this.options);
  }

  /**
   * Abre una modal en formato extra grande
   * @param content Contenido HTML
   */
  openXl(content: any) {
    this.options.size = 'xl';
    return this.modalService.open(content, this.options);
  }

  /**
   * Abre una modal en formato pantalla completa
   * @param content Contenido HTML
   */
  openXxl(content: any) {
    this.options.size = 'xxl';
    return this.modalService.open(content, this.options);
  }
}
