import { NgxUiLoaderConfig, POSITION, SPINNER, PB_DIRECTION } from 'ngx-ui-loader';

export const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  bgsPosition: POSITION.centerCenter,
  bgsSize: 40,
  bgsType: SPINNER.rectangleBounce,
  fgsType: SPINNER.chasingDots,
  hasProgressBar: true,
  pbDirection: PB_DIRECTION.leftToRight,
  pbThickness: 3,
  text: 'Cargando',
  textPosition: POSITION.centerCenter,
  textColor: '#C0C0C0'
};
