import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgxUiLoaderHttpModule, NgxUiLoaderRouterModule, NgxUiLoaderModule } from 'ngx-ui-loader';

// Configs
import { CustomNgbModalConfig } from 'src/app/core/configs/ngbmodal/ngbmodal-config';
import { ngxUiLoaderConfig } from './configs/ngx-loader-config';

// Interceptors
import { RequestInterceptor } from './interceptors/request.interceptor';
import { ErrorInterceptor } from './interceptors/error.interceptor';

// Guards
import { AuthGuard } from './guards/auth.guard';
import { RoleGuard } from './guards/role.guard';

// Services
import { ClientService } from './services/client.service';
import { LocalStorageService } from './services/local-storage.service';
import { AuthService } from './services/auth.service';
import { AdministratorService } from './services/administrator.service';
import { SidebarService } from './services/sidebar.service';
import { SummaryService } from './services/summary.service';

@NgModule({
  imports: [
    CommonModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    NgxUiLoaderRouterModule,
    NgxUiLoaderHttpModule,
  ],
  exports: [
    NgxUiLoaderModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    AuthGuard,
    RoleGuard,
    ClientService,
    AuthService,
    LocalStorageService,
    AdministratorService,
    SidebarService,
    CustomNgbModalConfig,
    SummaryService
  ],
})
export class CoreModule {}
