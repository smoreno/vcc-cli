import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { LocalStorageService } from '../services/local-storage.service';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(
    private router: Router,
    private lsService: LocalStorageService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const currentRole = this.lsService.getValue('role');
    console.log(currentRole);
    const pathByRole = {
      Administrador: ['administrator', 'system-values', 'global-settings', 'administrator-settings'],
      // Doctor: ['summary', 'queries', 'calendar', 'patient', 'document', 'billing', 'notification', 'diary', 'doctor-settings', 'search'],
      CNatural: ['summary'],
      CLegal: ['summary'],
      Driver: ['summary']
    };

    if (pathByRole[currentRole].includes(route.routeConfig.path)) {
      return true;
    } else {
      this.router.navigate(['/administrator/' + pathByRole[currentRole][0]]);
      return false;
    }
  }
}
