import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Administrator } from '../../shared/interfaces/administrator';

@Injectable()
export class AdministratorService {
  /**
   * Service builder AdministratorService
   * @param http HTTP request service
   */
  constructor(private http: HttpClient) { }

  updatePhoto(data: any) {
    return this.http.post(`${environment.apiRoot}/administrator/upload-photo`, data);
  }

  /**
   * Get all administrator
   */
  listAdministrators() {
    return this.http.get(`${environment.apiRoot}/administrator`);
  }

  /**
   * Get one administrator for setting
   * @param id administrator Setting to find
   */
  allSetting(id: number) {
    return this.http.get(`${environment.apiRoot}/administrator/${id}`);
  }

  /**
   * Get one administrator for detail
   * @param id Administrator to find
   */
  detail(id: number) {
    return this.http.get(`${environment.apiRoot}/administrator/${id}/detail`);
  }

  /**
   * Get one administrator
   * @param id Administrator for consult
   */
  byId(id: number) {
    return this.http.get(`${environment.apiRoot}/administrator/${id}`);
  }

  /**
   * Create a new administrator
   * @param administrator Object of the new administrator
   */
  create(administrator: Administrator) {
    return this.http.post(`${environment.apiRoot}/administrator`, administrator);
  }

  /**
   * Update a administrator
   * @param administrator Object of the administrator to update
   */
  update(administrator: Administrator) {
    return this.http.put(`${environment.apiRoot}/administrator`, administrator);
  }

  /**
   * Delete a administrator
   * @param id Administrator for delete
   */
  removeAdministrator(id: number) {
    return this.http.delete(`${environment.apiRoot}/administrator/${id}`);
  }

  /**
   * Update a profile
   * @param administrator Object of the administrator to update
   */
  updateProf(administrator: Administrator) {
    return this.http.patch(`${environment.apiRoot}/update-profile`, administrator);
  }

  /**
   * Update a password
   * @param pass New password to update
   */
  updatePass(pass) {
    return this.http.post(`${environment.apiRoot}/change-password`, pass);
  }

  /**
   * Update a notifications
   * @param administrator Object of the administrator to update
   */
  updateNoti(administrator: Administrator) {
    return this.http.patch(`${environment.apiRoot}/update-notifications`, administrator);
  }
}
