import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Client } from '../../shared/interfaces/client';

@Injectable()
export class ClientService {
  /**
   * Service builder ClientService
   * @param http HTTP request service
   */
  constructor(private http: HttpClient) {}

  /**
   * Get all clients
   */
  listClients() {
    return this.http.get(`${ environment.apiRoot }/client`);
  }

  /**
   * Get one client for setting
   * @param id Client Setting to find
   */
  allSettingByClientId(id: number) {
    return this.http.get(`${ environment.apiRoot }/client/${ id }`);
  }

  /**
   * Update a profile
   * @param client Object of the client to update
   */
  updateProfile(client: Client) {
    return this.http.patch(`${ environment.apiRoot }/client/update-profile`, client);
  }

  /**
   * Update a notifications
   * @param client Object of the client to update
   */
  updateNotifications(client: Client) {
    return this.http.patch(`${ environment.apiRoot }/client/update-notifications`, client);
  }

  /**
   * Get one client for detail
   * @param id Client to find
   */
  detail(id: number) {
    return this.http.get(`${ environment.apiRoot }/client/${ id }/detail`);
  }

  /**
   * Get one client
   * @param id client for consult
   */
  byId(id: number) {
    return this.http.get(`${ environment.apiRoot }/client/${ id }`);
  }

  /**
   * Create a new client
   * @param client Object of the new client
   */
  create(client: Client) {
    return this.http.post(`${ environment.apiRoot }/client`, client);
  }

  /**
   * Update a client
   * @param client Object of the client to update
   */
  update(client: Client) {
    return this.http.put(`${ environment.apiRoot }/client`, client);
  }

  /**
   * Delete a client
   * @param id Client for delete
   */
  removeClient(id: number) {
    return this.http.delete(`${ environment.apiRoot }/client/${ id }`);
  }
}
