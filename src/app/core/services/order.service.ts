import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Order } from 'src/app/shared/interfaces/order';

@Injectable()
export class OrderService {
    /**
     * Service builder OrderService
     * @param http HTTP request service
     */
    constructor(private http: HttpClient) { }

    /**
     * Get all orders
     */
    listOrders() {
        return this.http.get(`${environment.apiRoot}/client/orders`);
    }

    /**
     * Get one order for detail
     * @param id order to find
     */
    detail(id: number) {
        return this.http.get(`${environment.apiRoot}/client/${id}/order/`);
    }

    /**
     * Create a new order
     * @param order Object of the new order
     */
    create(order: Order) {
        return this.http.post(`${environment.apiRoot}/client/order`, order);
    }


    /**
     * Update a Order
     * @param order Object of the Order to update
     */
    update(order: Order) {
        return this.http.patch(`${environment.apiRoot}/client/order`, order);
    }

    /**
     * Delete a order
     * @param id order for delete
     */
    removeOrder(id: number) {
        return this.http.delete(`${environment.apiRoot}/client/order/${id}`);
    }

    /**
     * Create a new patient
     * @param patient Object of the new patient
     */
    updatePhotoQuery(photos: any) {
        return this.http.post(`${environment.apiRoot}/upload/photo-patient`, photos);
    }
}
