import { Injectable } from '@angular/core';

@Injectable()
export class SidebarService {
  public toggled = false;
  // tslint:disable-next-line:variable-name
  public _hasBackgroundImage = true;
  public menus = [{
    title: 'Menú',
    type: 'header'
  }, {
    title: 'Resumen',
    icon: 'fab fa-buffer',
    active: false,
    type: 'simple',
    url: '../summary'
  }, {
    title: 'Ordenes',
    icon: 'fa fa-id-card',
    active: false,
    type: 'simple',
    url: '../orders'
  }, {
    title: 'Calendario',
    icon: 'fa fa-calendar-alt',
    active: false,
    type: 'simple',
    url: '../calendar'
  }, {
    title: 'Documentos',
    icon: 'fa fa-file-medical',
    active: false,
    type: 'simple',
    url: '../document'
  }];
  public menusA = [{
    title: 'Menú',
    type: 'header'
  }, {
    title: 'Clientes',
    icon: 'fa fa-id-card',
    active: false,
    type: 'simple',
    url: '../client'
  }, {
    title: 'Administradores',
    icon: 'fa fa-calendar-alt',
    active: false,
    type: 'simple',
    url: '../administrator'
  }, {
    title: 'Ordenes',
    icon: 'fa fa-id-card',
    active: false,
    type: 'simple',
    url: '../orders'
  },{
    title: 'Valores del Sistema',
    icon: 'fa fa-user',
    active: false,
    type: 'simple',
    url: '../system-values',
  }, {
    title: 'Ajuste Globales',
    icon: 'fa fa-file-medical',
    active: false,
    type: 'simple',
    url: '../global-settings'
  }];

  toggle() {
    this.toggled = !this.toggled;
  }

  getSidebarState() {
    return this.toggled;
  }

  setSidebarState(state: boolean) {
    this.toggled = state;
  }

  getMenuList() {
    return this.menus;
  }
  getMenuAList() {
    return this.menusA;
  }

  get hasBackgroundImage() {
    return this._hasBackgroundImage;
  }

  set hasBackgroundImage(hasBackgroundImage) {
    this._hasBackgroundImage = hasBackgroundImage;
  }
}
