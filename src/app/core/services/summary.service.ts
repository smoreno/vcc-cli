import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class SummaryService {
    /**
     * Service builder GlobalsSettingsService
     * @param http HTTP request service
     */
    constructor(private http: HttpClient) { }

    /**
     * getDataSummary
     * @param until
     * @param since
     */
    get(until: string, since: string) {
        return this.http.get(`${environment.apiRoot}/summary/${until}/${since}`);
    }


}
