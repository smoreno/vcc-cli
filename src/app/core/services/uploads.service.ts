import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class UploadsService {
  /**
   * Service builder GlobalsSettingsService
   * @param http HTTP request service
   */
  constructor(private http: HttpClient) {}

  /**
   * Upload avatar
   * @param avatar image in base64 to upload
   */
  upload(data: any) {
    return this.http.post(`${environment.apiRoot}/upload`, data);
  }

  /**
   * Remove avatar
   */
  remove(data: any) {
    return this.http.post(`${environment.apiRoot}/upload/remove`, data);
  }

  /**
   * check avatar or logo
   */
  check(data: any) {
    return this.http.post(`${environment.apiRoot}/upload/check`, data);
  }
}
