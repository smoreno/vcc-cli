import { Component, OnInit, ChangeDetectorRef, ElementRef, ViewChild, Input } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { IResponse } from 'src/app/shared/interfaces/response';
import { UploadsService } from 'src/app/core/services/uploads.service';

@Component({
  selector: 'app-avatar-card',
  templateUrl: './avatar-card.component.html',
  styleUrls: ['./avatar-card.component.scss']
})
export class AvatarCardComponent implements OnInit {
  public submitted = false;
  public isImageToUpload = false;
  public haveToUpdate = false;
  public editFile = true;
  public removeUpload = false;
  public administratorId: number;

  constructor(
    public fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private uploadsService: UploadsService,
    private lsService: LocalStorageService) {}

  /*
  *This input will allow me to determine that
  *the component will upload 'avatar' or a 'logo'
  */
  @Input() public type: string;
  /*
  *for udate a external avatar or logo , diferrent to user loggued
  */
  @Input() public userId: number = null;

  /*##################### Registration Form #####################*/
  registrationForm = this.fb.group({
    photo: [null],
    userId: this.userId,
    type: this.type
  });

  /*########################## File Upload ########################*/
  //@ViewChild('fileInput') el: ElementRef;
  imageUrl: any = 'assets/images/camera.png';

  ngOnInit() {
    this.check(this.type);
  }

  uploadFile(event: any) {
    const reader = new FileReader(); // HTML5 FileReader API
    const file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      reader.readAsDataURL(file);

      // When file uploads set it to file formcontrol
      reader.onload = () => {
        this.imageUrl = reader.result;
        this.registrationForm.patchValue({
          photo: reader.result,
          userId: this.userId,
          type: this.type
        });
        this.isImageToUpload = true;
        this.haveToUpdate = true;
        this.editFile = false;
        this.removeUpload = true;
      };
      // ChangeDetectorRef since file is loading outside the zone
      this.cd.markForCheck();
    }
  }

  removeUploadedFile() {
    this.uploadsService.remove(this.registrationForm.value).subscribe(
      (data) => {
        //const newFileList = Array.from(this.el.nativeElement.files);
        this.imageUrl = 'assets/images/camera.png';
        this.editFile = true;
        this.removeUpload = false;
        this.registrationForm.patchValue({
          photo: [null],
          userId: this.userId,
          type: this.type
        });
        this.isImageToUpload = false;
        this.lsService.setValue('photo', this.imageUrl);
      },
      (error) => console.log(error)
    );
  }

  save() {
    this.uploadsService.upload(this.registrationForm.value).subscribe(
      (data: IResponse) => {
        if (data.response.photo) {
          this.imageUrl = data.response.photo;
          this.haveToUpdate = false;
        } else {
          this.imageUrl = data.response.value;
          this.haveToUpdate = false;
        }
        this.lsService.setValue('photo', this.imageUrl);
      },
      (error) => this.haveToUpdate = false
    );
  }

  checkIfUserHaveAvatar() {
    this.registrationForm.patchValue({
      photo: [null],
      userId: this.userId,
      type: this.type
    });

    // Get id
    this.administratorId = parseInt(this.lsService.getValue('id'));

    // Get of settings
    this.uploadsService.check(this.registrationForm.value).subscribe(
      (data: IResponse) => {
        if (data.response.photo !== null) { // have avatar
          this.registrationForm.patchValue({
            photo: data.response.photo,
            userId: this.userId,
            type: this.type
          });
          this.imageUrl = data.response.photo;
          this.isImageToUpload = true;
        } else { // haven't avatar
          this.registrationForm.patchValue({
            photo: [null],
            userId: this.userId,
            type: this.type
          });
          this.isImageToUpload = false;
        }
      },
      err => console.log(err)
    );
  }

  checkIfUserHaveLogo() {
    this.registrationForm.patchValue({
      photo: [null],
      userId: this.userId,
      type: this.type
    });

    // Get id
    this.administratorId = parseInt(this.lsService.getValue('id'));

    // Get of settings
    this.uploadsService.check(this.registrationForm.value).subscribe(
      (data: IResponse) => {
        // have logo
        if (String(data.response.value).toLowerCase() !== 'null') {
          this.registrationForm.patchValue({
            photo: data.response.value,
            userId: this.userId,
            type: this.type
          });
          this.imageUrl = data.response.value;
          this.isImageToUpload = true;
        } else { // haven't logo
          this.registrationForm.patchValue({
            photo: [null],
            userId: this.userId,
            type: this.type
          });
          this.isImageToUpload = false;
        }
      },
      err => console.log(err)
    );
  }

  check(type: string) {
    switch (type) {
      case 'avatar': this.checkIfUserHaveAvatar(); break;
      case 'logo': this.checkIfUserHaveLogo(); break;
    }
  }
}
