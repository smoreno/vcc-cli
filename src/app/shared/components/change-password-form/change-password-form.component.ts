import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Authentication } from 'src/app/shared/interfaces/authentication';
import { MustMatch } from 'src/app/shared/helpers/must-match.validator';
import { IResponse } from 'src/app/shared/interfaces/response';

@Component({
  selector: 'app-change-password-form',
  templateUrl: './change-password-form.component.html',
  styleUrls: ['./change-password-form.component.scss']
})
export class ChangePasswordFormComponent implements OnInit {
  public changePasswordForm: FormGroup;
  public auth: Authentication = {};
  public submitted = false;
  @Output() passwordChanged = new EventEmitter<boolean>();

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private toastrService: ToastrService,
  ) {}

  ngOnInit() {
    // Build form
    this.changePasswordForm = this.formBuilder.group({
      newPassword: ['', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(10),
      ]],
      confirmPassword: ['', Validators.required ],
    }, {
      validator: MustMatch('newPassword', 'confirmPassword'),
    });
  }

  get f() { return this.changePasswordForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.changePasswordForm.invalid) {
      return;
    }

    // Set object
    this.auth.password = this.changePasswordForm.get('newPassword').value;

    // Send request
    this.authService.changePassword(this.auth).subscribe(
      (data: IResponse) => {
        this.toastrService.success(data.response);
        this.passwordChanged.emit(true);
      },
      err => console.log(err)
    );
  }
}
