import { Router } from '@angular/router';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-client-card',
  templateUrl: './client-card.component.html',
  styleUrls: ['./client-card.component.scss']
})
export class ClientCardComponent {
  /*
  * client data
  */
  @Input() public client: any;
  /*
  * parent name
  * client or orders
  */
  @Input() public clientName: string;

  constructor(private router: Router) {}

  goToDetail(id) {
    switch (this.clientName) {
      case 'client':
        /*
        * TODO:
        * mandar a la info del cliente
        */
        this.router.navigateByUrl(`administrator/client/info/${id}`);
        break;
      case 'order':
        this.router.navigateByUrl(`administrator/orders/info/${id}`);
        break;
    }
  }
}
