import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.scss']
})
export class DatatableComponent implements OnInit {
  @Input() public settings: any;
  @Input() public options: any;
  @Input() public data: any;
  @Output() public selectedOption = new EventEmitter<object>();
  public filterQuery = '';
  public rowsOnPage = 5;
  public sortBy = '';
  public sortByName = '';
  public sortOrder = 'asc';
  public rows = 1;
  public rowsMessage: string;

  ngOnInit() {
    this.rowsMessage = environment.messages.withoutData;
  }

  /**
   * Changing the value to look for
   * @param newValue New value
   */
  changeSortBy(newValue: string, title: string) {
    if(newValue == null){
      this.sortByName = '';
      this.filterQuery = ''
    }
    else{
      this.sortBy = newValue;
      this.sortByName = title;
    }
    
  }

  option(current: any, id: any, idUser?: any) {
    current.idElement = id;
    if (idUser) {
      current.idUser = idUser;
    }
    this.selectedOption.emit(current);
  }
}
