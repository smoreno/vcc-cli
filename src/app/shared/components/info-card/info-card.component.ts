import { Component, Input } from '@angular/core';

// Interfaces
import { InfoCardOptions } from '../../interfaces/info-card-options';
import { Router } from '@angular/router';

@Component({
  selector: 'app-info-card',
  templateUrl: './info-card.component.html',
  styleUrls: ['./info-card.component.scss']
})
export class InfoCardComponent {
  @Input() options: InfoCardOptions;

  constructor(private router: Router) {}

  goTo(url: string) { this.router.navigate([url]); }
}
