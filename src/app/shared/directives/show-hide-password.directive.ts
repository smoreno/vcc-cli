import { Directive, ElementRef } from '@angular/core';

@Directive({ selector: '[appShowHidePassword]' })
export class ShowHidePasswordDirective {
  private show = false;

  constructor(private el: ElementRef) {
    this.setup();
  }

  toggle(a: HTMLElement) {
    this.show = !this.show;
    const child = a.firstElementChild;
    if (this.show) {
      this.el.nativeElement.setAttribute('type', 'text');
      a.setAttribute('title', 'Ocultar contraseña');
      child.classList.remove('fa-eye');
      child.classList.add('fa-eye-slash');
    } else {
      this.el.nativeElement.setAttribute('type', 'password');
      a.setAttribute('title', 'Mostrar contraseña');
      child.classList.remove('fa-eye-slash');
      child.classList.add('fa-eye');
    }
  }

  setup() {
    const parent = this.el.nativeElement.parentNode;

    // Link
    const a = document.createElement('a');
    a.setAttribute('style', 'right: 0;');
    a.setAttribute('title', 'Mostrar contraseña');
    a.classList.add('position-absolute');
    a.classList.add('pr-4');
    a.classList.add('pt-3');

    // Icon
    const i = document.createElement('i');
    i.classList.add('fas');
    i.classList.add('fa-eye');

    // Event
    a.addEventListener('click', event => this.toggle(a) );

    // Append Childs
    a.appendChild(i);
    parent.appendChild(a);
  }
}
