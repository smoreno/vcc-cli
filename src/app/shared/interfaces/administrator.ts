export interface Administrator {
  id?: number;
  firstName: string;
  lastName: string;
  email: string;
  photo?: string;
  password?: string;
}
