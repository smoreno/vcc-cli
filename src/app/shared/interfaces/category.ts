export interface Category {
  id: number;
  name: string;
  description: string;
  editable: boolean;
  subcategories: any[];
}
