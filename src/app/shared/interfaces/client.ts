export interface Client {
  id?: number;
  ine?: string;
  firstName?: string;
  lastName?: string;
  gender?: string;
  address?: string;
  email?: string;
  photo?: string;
  phone?: string;
  password?: string;
}
