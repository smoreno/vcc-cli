export interface DatatableOptions {
  id: string;
  title: string;
  idElement: string;
  idUser? : string;
}
