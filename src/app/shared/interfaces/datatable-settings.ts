export interface DatatableSettings {
  id: string;
  title: string;
  filter: boolean;
}
