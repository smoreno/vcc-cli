export interface InfoCardOptions {
  icon: string;
  title: string;
  subtitle: string;
  actionTitle: string;
  startColor: string;
  endColor: string;
  backgroundWhite: boolean;
  path: string;
}
