// import { CLient } from './client';

export interface Order {
  id?: number;
  nro?: string;
  idService?: number;
  idStock?: number;
  idSate?: number;
  idOrigin?: number;
  idDestination?: number;
  idVehicle?: number;
  manualVehicle?: string;
  transferDate?: string;
  hourTransfer?: string;
  observation?: string;
  color?: string;
  name?: string;
  responsibleEmail?: string;
  phone?: string;
  method?: string;

  // CHECK IN
  idOrder: number;
  tipoGasolina: number;
  kilometraje: string;
  manual: any;
  tarjeta: any;
  permiso: any;
  talon: any;
  carnet: any;
  estuche: any;
  nroLlave: any;
  tapa: any;
  gato: any;
  llaveCruz: any;
  kitInflado: any;
  botiquin: any;
  taponMotor: any;
  herramienta: any;
  compresor: any;
  triangulo: any;
  taponGas: any;
  antena: any;
  extintor: any;
  encendedor: any;
  portavasos: any;
  radio: any;
  cinturón: any;
  alfombra: any;
  alzavidrio: any;
  cerradura: any;
  ac: any;
  cierreCentral: any;
  alarma: any;
  gps: any;
  sillaBebé: any;
  luces: any;
  aseoInt: any;
  aseoExt: any;
  retrovisor: any;
  espejoLaterales: any;
  serialBateria: any;
  estadoParabrisa: any;
  objetosDejado: any;
  obsDerecho: any;
  obsIzquierdo: any;
  obsPosterior: any;
}

// export interface Query {
//   id?: number;
//   origin?: boolean;
//   opinionNose?: string;
//   frontPicture?: string;
//   basePicture?: string;
//   profilePicture?: string;
//   customPicture?: string;
//   date?: string;
//   patient?: Patient;
// }
