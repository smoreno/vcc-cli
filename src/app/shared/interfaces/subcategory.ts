export interface Subcategory {
  catId?: number;
  id?: number;
  name?: string;
}
