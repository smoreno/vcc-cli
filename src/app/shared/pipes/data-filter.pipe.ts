import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({ name: 'dataFilter' })
export class DataFilterPipe implements PipeTransform {
  /**
   * Data filter
   * @param array Data arrangement
   * @param type Type of data
   * @param query Query
   */
  transform(array: any[], type: string, query: string): any {
    return (query) ? _.filter(array, row => row[type].indexOf(query) > -1) : array;
  }
}
