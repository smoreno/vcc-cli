import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DataTableModule } from 'angular-6-datatable';
import { NgbModule, NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ToastrModule } from 'ngx-toastr';
import { FullCalendarModule } from '@fullcalendar/angular';
import { NgxPrintModule } from 'ngx-print';
import { FileUploadModule } from '@iplab/ngx-file-upload';

// Pipes
import { DataFilterPipe } from './pipes/data-filter.pipe';

// Directives
import { ShowHidePasswordDirective } from './directives/show-hide-password.directive';

// Components
import { AvatarCardComponent } from './components/avatar-card/avatar-card.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { OnDevelopmentComponent } from './components/on-development/on-development.component';
import { ChangePasswordFormComponent } from './components/change-password-form/change-password-form.component';
import { DatatableComponent } from './components/datatable/datatable.component';
import { InfoCardComponent } from './components/info-card/info-card.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    DataTableModule,
    BsDropdownModule.forRoot(),
    NgbModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-top-right',
      progressBar: true,
      preventDuplicates: true,
      resetTimeoutOnDuplicate: true,
      closeButton: true
    }),
    NgbDatepickerModule,
    FullCalendarModule,
    NgxPrintModule,
    FileUploadModule,
  ],
  exports: [
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    PageNotFoundComponent,
    OnDevelopmentComponent,
    ChangePasswordFormComponent,
    DatatableComponent,
    AvatarCardComponent,
    InfoCardComponent,
    ShowHidePasswordDirective,
    DataFilterPipe,
    DataTableModule,
    NgbModule,
    FullCalendarModule,
    NgxPrintModule,
    FileUploadModule
  ],
  declarations: [
    AvatarCardComponent,
    PageNotFoundComponent,
    OnDevelopmentComponent,
    ChangePasswordFormComponent,
    ShowHidePasswordDirective,
    InfoCardComponent,
    DataFilterPipe,
    DatatableComponent
  ],
})
export class SharedModule {}
