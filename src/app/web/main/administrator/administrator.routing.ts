import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import { RoleGuard } from 'src/app/core/guards/role.guard';

const routes: Routes = [{
  path: '',
  redirectTo: 'summary',
  pathMatch: 'full'
}, {
  path: 'summary',
  canActivate: [AuthGuard, RoleGuard],
  component: IndexComponent,
  loadChildren: '../../modules-administrator/summary/summary.module#SummaryModule'
},{
  path: 'administrator-settings',
  canActivate: [AuthGuard, RoleGuard],
  component: IndexComponent,
  loadChildren: '../../modules-system/administrator-settings/administrator-settings.module#AdministratorSettingsModule'
},  {
  path: 'administrator',
  canActivate: [AuthGuard, RoleGuard],
  component: IndexComponent,
  loadChildren: '../../modules-system/administrator/administrator.module#AdministratorModule'
} ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministratorRoutingModule {}
