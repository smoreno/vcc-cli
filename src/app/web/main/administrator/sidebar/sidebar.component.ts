import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { SidebarService } from '../../../../core/services/sidebar.service';
import { LocalStorageService } from '../../../../core/services/local-storage.service';
import { AuthService } from '../../../../core/services/auth.service';
import { Router } from '@angular/router';
import { IResponse } from '../../../../shared/interfaces/response';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  animations: [
    trigger('slide', [
      state('up', style({ height: 0 })),
      state('down', style({ height: '*' })),
      transition('up <=> down', animate(200))
    ])
  ]
})
export class SidebarComponent implements OnInit {
  @Input() public visibility: string;
  @Output() public changeVisibility = new EventEmitter<string>();
  public userName: string;
  public userEmail: string;
  public userPhoto: string;
  public modeAdmin: boolean;
  public role: string;
  public toggleSidebar = 'show';
  menus = [];
  menusA = [];

  constructor(
    public sidebarservice: SidebarService,
    private lsService: LocalStorageService,
    private authService: AuthService,
    private router: Router) {
    this.menus = sidebarservice.getMenuList();
    this.menusA = sidebarservice.getMenuAList();
  }

  ngOnInit() {
    this.role = this.lsService.getValue('role');
    this.modeAdmin = (this.lsService.getValue('role') === 'Administrador');
    this.userName = this.lsService.getValue('name');
    this.userEmail = this.lsService.getValue('email');
    this.userPhoto = this.lsService.getValue('photo');
    this.userPhoto = (this.userPhoto !== 'null') ? this.userPhoto : 'assets/images/avatar.png';
  }

  toggle(currentMenu: any) {
    if (currentMenu.type === 'dropdown') {
      this.menus.forEach(element => {
        if (element === currentMenu) {
          currentMenu.active = !currentMenu.active;
        } else {
          element.active = false;
        }
      });
    }
  }

  getState(currentMenu: any) {
    return (currentMenu.active) ? 'down' : 'up';
  }

  hasBackgroundImage() {
    return this.sidebarservice.hasBackgroundImage;
  }

  toggleBackgroundImage() {
    this.sidebarservice.hasBackgroundImage = !this.sidebarservice.hasBackgroundImage;
  }

  toggled() {
    switch (this.visibility) {
      case 'none':
        this.visibility = 'show';
        this.changeVisibility.emit(this.visibility);
        break;
      case 'show':
        this.visibility = 'none';
        this.changeVisibility.emit(this.visibility);
        break;
      default:
        this.visibility = 'show';
        this.changeVisibility.emit(this.visibility);
        break;
    }
  }

  logout() {
    this.authService.logout().subscribe(
      (data: IResponse) => {
        this.lsService.clearStorage();
        this.router.navigate(['/auth/login']);
      },
      err => {
        if (err.name === 'TokenExpiredError') {
          this.lsService.clearStorage();
          this.router.navigate(['/auth/login']);
        } else {
          console.log(err);
        }
      }
    );
  }
}
