import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationRoutingModule } from './authentication.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { LoginComponent } from './login/login.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { RequestChangePasswordComponent } from './request-change-password/request-change-password.component';
import { ValidateTokenComponent } from './validate-token/validate-token.component';
import { RegisterClientNaturalComponent } from './register/register-client-natural/register-client-natural.component';
import { RegisterClientLegalComponent } from './register/register-client-legal/register-client-legal.component';

@NgModule({
  declarations: [
    LoginComponent,
    RequestChangePasswordComponent,
    ChangePasswordComponent,
    ValidateTokenComponent,
    RegisterClientNaturalComponent,
    RegisterClientLegalComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AuthenticationRoutingModule,
  ],
  exports: [ChangePasswordComponent]
})
export class AuthenticationModule {}
