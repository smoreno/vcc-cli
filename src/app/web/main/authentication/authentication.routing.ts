import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { LoginComponent } from './login/login.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { RequestChangePasswordComponent } from './request-change-password/request-change-password.component';
import { ValidateTokenComponent } from './validate-token/validate-token.component';
import { RegisterClientNaturalComponent } from '../authentication/register/register-client-natural/register-client-natural.component';
import { RegisterClientLegalComponent } from '../authentication/register/register-client-legal/register-client-legal.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'request-change-password', component: RequestChangePasswordComponent },
  { path: 'change-password', component: ChangePasswordComponent },
  { path: 'validate/:token', component: ValidateTokenComponent },
  { path: 'register-client-natural', component: RegisterClientNaturalComponent },
  { path: 'register-client-legal', component: RegisterClientLegalComponent }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthenticationRoutingModule {}
