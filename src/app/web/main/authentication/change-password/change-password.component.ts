import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {
  constructor(
    private lsService: LocalStorageService,
    private router: Router) {}

  ngOnInit() {
    // Validate token
    const token = this.lsService.getValue('token');
    if (!token) {
      this.router.navigate(['/auth/login']);
    }
  }

  verifyPasswordChanged(event) {
    if (event) {
      this.lsService.clearStorage();
      this.router.navigate(['/auth/login']);
    }
  }
}
