import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Authentication } from 'src/app/shared/interfaces/authentication';
import { AuthService } from 'src/app/core/services/auth.service';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { IResponse } from 'src/app/shared/interfaces/response';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public auth: Authentication = {};
  public submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private lsService: LocalStorageService,
    private router: Router) {}

  ngOnInit() {
    // Validate token
    const token = this.lsService.getValue('token');
    console.log(token);
    if (token) {
      this.router.navigate(['/administrator']);
    }

    // Build form
    this.loginForm = this.formBuilder.group({
      email: ['', [
        Validators.required,
        Validators.pattern(/\b\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+\b/)
      ]],
      password: ['', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(10),
      ]],
    });
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }

    // Set object
    this.auth.email = this.loginForm.get('email').value;
    this.auth.password = this.loginForm.get('password').value;

    // Send request
    this.authService.login(this.auth).subscribe(
      (data: IResponse) => {
        this.lsService.setValue('token', (data.response.role) ? data.response.token : data.response.user.token);
        this.lsService.setValue('id', (data.response.role) ? data.response.id : data.response.user.id);
        this.lsService.setValue('clientId', (data.response.role) ? null : data.response.id);
        this.lsService.setValue('firstName', (data.response.role) ? data.response.firstName : data.response.user.firstName);
        this.lsService.setValue('lastName', (data.response.role) ? data.response.lastName : data.response.user.lastName);
        this.lsService.setValue('name', this.lsService.getValue('firstName') + ' ' + this.lsService.getValue('lastName'));
        this.lsService.setValue('email', (data.response.role) ? data.response.email : data.response.user.email);
        this.lsService.setValue('photo', (data.response.role) ? data.response.photo : data.response.user.photo);
        this.lsService.setValue('role', (data.response.role) ? data.response.role : data.response.user.role);

        if (this.lsService.getValue('role') === 'Administrador') {
          this.router.navigate(['/administrator']);
        } else if (this.lsService.getValue('role') === 'CNatural') {
          this.router.navigate(['/administrator/summary']);
        } else if (this.lsService.getValue('role') === 'CLegal') {
          this.router.navigate(['/administrator/summary']);
        } else if (this.lsService.getValue('role') === 'Driver') {
          this.router.navigate(['/administrator/summary']);
        } else {
          this.router.navigate(['/administrator/not-found']);
        }
      },
      err => console.log(err)
    );
  }
}
