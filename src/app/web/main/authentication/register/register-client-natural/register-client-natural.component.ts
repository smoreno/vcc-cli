import { Component, OnInit, Output, ViewChild, EventEmitter, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MustMatch } from 'src/app/shared/helpers/must-match.validator';
import { Router } from '@angular/router';

// Interfaces
import { Client } from 'src/app/shared/interfaces/client';
import { IResponse } from '../../../../../shared/interfaces/response';

import { ClientService } from '../../../../../core/services/client.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register-client-natural',
  templateUrl: './register-client-natural.component.html',
  styleUrls: ['./register-client-natural.component.scss']
})
export class RegisterClientNaturalComponent implements OnInit {
  public registerForm: FormGroup;
  public client: Client = {};
  public submitted = false;

  /**
   * Class constructor
   * @param fb FormGroup builder
   * @param clientService Doctor consultation service
   * @param toastr Message service
   */
  constructor(
    private fb: FormBuilder,
    private clientService: ClientService,
    private toastrService: ToastrService,
    private router: Router) {}

  /**
   * Building the FormBuilder
   */
  ngOnInit() {
    this.submitted = false;

    // Building the form
    this.registerForm = this.fb.group({
      ine: ['', [ Validators.required ] ],
      firstName: ['', [ Validators.required ] ],
      lastName: ['', [ Validators.required ] ],
      gender: ['', [ Validators.required ] ],
      address: ['', [ Validators.required ] ],
      email: ['', [
        Validators.required,
        Validators.pattern(/\b\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+\b/)
      ] ],
      phone: ['',
      [ Validators.required,
        Validators.minLength(10),
        Validators.maxLength(11),
      ]],
      password: ['', [ Validators.required ] ],
      confirmPassword: ['', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(10),
      ]],
    }, {
      validator: MustMatch('password', 'confirmPassword'),
    });
  }

  /**
   * Selector to access the fields of the doctor object
   */
  get f() { return this.registerForm.controls; }

  /**
   * Form Validation
   */
  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }

    // Verifico el método a utilizar
    const method = 'create';

    // Cargo los datos del objeto a enviar
    this.client.ine = this.registerForm.get('ine').value;
    this.client.firstName = this.registerForm.get('firstName').value;
    this.client.lastName = this.registerForm.get('lastName').value;
    this.client.gender = this.registerForm.get('gender').value;
    this.client.address = this.registerForm.get('address').value;
    this.client.email = this.registerForm.get('email').value;
    this.client.password = this.registerForm.get('password').value;
    this.client.phone = this.registerForm.get('phone').value;

    this.clientService[method](this.client).subscribe(
      (data: IResponse) => {
        if (data.code === 200) {
          this.toastrService.success('Cliente creado con éxito');
          this.router.navigate(['/auth']);
        } else {
          this.toastrService.error(data.response.Message);
        }
      },
      err => console.log(err)
    );
  }
}
