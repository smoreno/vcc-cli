import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/core/services/auth.service';
import { Authentication } from 'src/app/shared/interfaces/authentication';
import { IResponse } from 'src/app/shared/interfaces/response';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-request-change-password',
  templateUrl: './request-change-password.component.html',
  styleUrls: ['./request-change-password.component.scss']
})
export class RequestChangePasswordComponent implements OnInit {
  public requestForm: FormGroup;
  public auth: Authentication = {};
  public submitted = false;
  public currentImage: string;
  public mailSent: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private toastrService: ToastrService) {}

  ngOnInit() {
    // Set current image
    this.currentImage = 'send-mail.png';

    // Set mailSent
    this.mailSent = false;

    // Build form
    this.requestForm = this.formBuilder.group({
      email: ['', [
        Validators.required,
        Validators.pattern(/\b\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+\b/)
      ]],
    });
  }

  get f() { return this.requestForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.requestForm.invalid) {
      return;
    }

    // Set object
    this.auth.email = this.requestForm.get('email').value;

    // Send request
    this.sendMail();
  }

  sendMail() {
    // Send request
    this.authService.requestChangePassword(this.auth).subscribe(
      (data: IResponse) => {
        // Show message
        this.toastrService.info(data.response);

        // Change image and mail send
        this.currentImage = 'open-mail.png';
        this.mailSent = true;
      },
      err => console.log(err)
    );
  }
}
