import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { IResponse } from 'src/app/shared/interfaces/response';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';

@Component({
  selector: 'app-validate-token',
  templateUrl: './validate-token.component.html',
  styleUrls: ['./validate-token.component.scss']
})
export class ValidateTokenComponent implements OnInit {
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private lsService: LocalStorageService) {}

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      // Get token
      const currentToken = params.get('token');

      if (currentToken) {
        // Validate token
        this.authService.validateToken(currentToken).subscribe(
          (data: IResponse) => {
            // Save token on LocalStorage
            this.lsService.setValue('token', currentToken);

            // Redirect to change password
            this.router.navigate(['/auth/change-password']);
          },
          err => this.router.navigate(['/auth/login'])
        );
      }
    });
  }
}
