import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OnlineRoutingModule } from './online.routing';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { MatStepperModule } from '@angular/material/stepper';
import { SharedModule } from 'src/app/shared/shared.module';
import { FileUploadModule } from '@iplab/ngx-file-upload';
import { MatIconModule } from '@angular/material/icon';

// Components
//import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    //HomeComponent,
  ],
  imports: [
    CommonModule,
    OnlineRoutingModule,
    ReactiveFormsModule,
    MatStepperModule,
    SharedModule,
    FileUploadModule,
    MatIconModule,
    FormlyBootstrapModule,
    FormlyModule.forRoot({
      validationMessages: [
        { name: 'required', message: 'This field is required' },
      ],
    }),
  ]
})
export class OnlineModule {}
