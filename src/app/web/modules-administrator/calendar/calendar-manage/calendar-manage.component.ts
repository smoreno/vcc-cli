import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { CustomNgbModalConfig } from 'src/app/core/configs/ngbmodal/ngbmodal-config';

@Component({
  selector: 'app-calendar-manage',
  templateUrl: './calendar-manage.component.html',
  styleUrls: ['./calendar-manage.component.scss']
})
export class CalendarManageComponent implements OnInit {
  //@ViewChild('content') contentModal: TemplateRef<any>;
  public title = '';
  public currentModal: any;

  constructor(
    private fb: FormBuilder,
    private modalService: CustomNgbModalConfig) {}

  ngOnInit() {
  }

  openModal() {
    //this.currentModal = this.modalService.openLg(this.contentModal);
    this.title = 'Agendar Cita';
  }

  closeModal() {
    this.currentModal.close();
  }
}
