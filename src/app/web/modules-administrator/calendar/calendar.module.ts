import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarRoutingModule } from './calendar.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { IndexComponent } from './index/index.component';
import { CalendarManageComponent } from './calendar-manage/calendar-manage.component';

@NgModule({
  declarations: [
    IndexComponent,
    CalendarManageComponent,
  ],
  imports: [
    CommonModule,
    CalendarRoutingModule,
    SharedModule,
  ]
})
export class CalendarModule {}
