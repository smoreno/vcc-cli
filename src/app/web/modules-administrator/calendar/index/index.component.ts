import { Component, OnInit, ViewChild } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGrigPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import listPlugin from '@fullcalendar/list';
import esLocale from '@fullcalendar/core/locales/es';
import { FullCalendarComponent } from '@fullcalendar/angular';
import bootstrapPlugin from '@fullcalendar/bootstrap';
import { EventInput } from '@fullcalendar/core';
import { CalendarManageComponent } from '../calendar-manage/calendar-manage.component';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
  //@ViewChild('fullcalendar') fullcalendar: FullCalendarComponent;
  //@ViewChild(CalendarManageComponent) manageComponent: CalendarManageComponent;
  calendarEvents: EventInput[] = [{
    title: 'Anthony Hurtado',
    start: '2019-11-01T01:30:00',
    end: '2019-11-01T02:30:00',
    backgroundColor: '#F0EEFA',
    textColor: '#705bcf',
    borderColor: '#F0EEFA',
  }, {
    title: 'Estefania Alvarez',
    start: '2019-11-01T02:30:00',
    end: '2019-11-01T03:30:00',
    backgroundColor: '#F0EEFA',
    textColor: '#705bcf',
    borderColor: '#F0EEFA'
  }, {
    title: 'Miguel Arreaza',
    start: '2019-11-04T10:30:00',
    end: '2019-11-04T12:30:00',
    backgroundColor: '#F0EEFA',
    textColor: '#705bcf',
    borderColor: '#F0EEFA'
  }, {
    title: 'Milennys Carrión',
    start: '2019-11-04T01:30:00',
    end: '2019-11-04T02:30:00',
    backgroundColor: '#F0EEFA',
    textColor: '#705bcf',
    borderColor: '#F0EEFA'
  }, {
    title: 'Zulay Perozo',
    start: '2019-11-06T09:30:00',
    end: '2019-11-06T10:30:00',
    backgroundColor: '#F0EEFA',
    textColor: '#705bcf',
    borderColor: '#F0EEFA'
  }, {
    title: 'Mirna Pico',
    start: '2019-11-06T14:30:00',
    end: '2019-11-06T16:30:00',
    backgroundColor: '#F0EEFA',
    textColor: '#705bcf',
    borderColor: '#F0EEFA'
  }];

  ngOnInit() {
    /*this.fullcalendar.defaultView = 'dayGridMonth';
    this.fullcalendar.themeSystem = 'bootstrap';
    this.fullcalendar.timeZone = 'America/Caracas';
    this.fullcalendar.locale = esLocale;
    this.fullcalendar.plugins = [dayGridPlugin, timeGrigPlugin, listPlugin, interactionPlugin, bootstrapPlugin];
    this.fullcalendar.weekends = true;
    this.fullcalendar.deepChangeDetection = true;
    this.fullcalendar.header = { left: 'prev, next', center: 'title', right: 'today' }; // timeGridWeek, timeGridDay,dayGridMonth, listWeek
    this.fullcalendar.events = this.calendarEvents;*/
  }

  /**
   * Call the modal to create a new administrators
   */
  new() {
    //this.manageComponent.openModal();
  }
}
