import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { OnDevelopmentComponent } from 'src/app/shared/components/on-development/on-development.component';

const routes: Routes = [{
  path: '',
  component: OnDevelopmentComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationRoutingModule {}
