import { Component, ViewChild, EventEmitter, Output, TemplateRef } from '@angular/core';
import { CustomNgbModalConfig } from 'src/app/core/configs/ngbmodal/ngbmodal-config';
import { ToastrService } from 'ngx-toastr';
import { IResponse } from 'src/app/shared/interfaces/response';
import { OrderService } from 'src/app/core/services/order.service';

@Component({
  selector: 'app-delete-order',
  templateUrl: './delete-order.component.html',
  styleUrls: ['./delete-order.component.scss']
})
export class DeleteOrderComponent {
  // Propiedades
  //@ViewChild('content') contentModal: TemplateRef<any>;
  @Output() updateOrderList = new EventEmitter();
  public currentModal: any;
  public orderId: number;

  /**
   * Class constructor
   * @param queryService Service of query
   * @param modalService Modal service
   */
  constructor(
    private modalService: CustomNgbModalConfig,
    private ordersService: OrderService,
    private toastrService: ToastrService) {}

  openModal(id: number): void {
    if (id) {
      this.orderId = id;
      // I open the modal
      //this.currentModal = this.modalService.openSm(this.contentModal);
    } else {
      this.toastrService.error('Ocurrio un error inesperado, por favor intente nuevamente');
    }
  }

  /**
   * Close the modal window
   * @param orderId order to delete
   */
  removeOrder(orderId: number) {
    this.ordersService.removeOrder(orderId).subscribe(
      (data: IResponse) => {
        if (data.response.affected === 1) {
          this.toastrService.success('Orden eliminada');

          this.currentModal.close();

          // I update the records
          this.updateOrderList.emit(true);

        } else {
          this.toastrService.error('Ocurrio un error inesperado, por favor intente nuevamente');
        }
      },
      err => console.log(err)
    );
  }

  /**
   * Close the modal window
   */
  closeModal() {
    this.currentModal.close();
  }
}
