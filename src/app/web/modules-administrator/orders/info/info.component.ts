import { ToastrService } from 'ngx-toastr';
import { OrderService } from '../../../../core/services/order.service';
import { ClientService } from './../../../../core/services/client.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Client } from 'src/app/shared/interfaces/client';
import { IResponse } from 'src/app/shared/interfaces/response';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {
  public client;
  constructor(
    private router: Router,
    private clientService: ClientService,
    private orderService: OrderService,
    private toastrService: ToastrService,
    private _router: ActivatedRoute) { }

  ngOnInit() {
    this.infoClient(+this._router.snapshot.paramMap.get('id'));
  }

  /**
   * Load the data in the case of the update
   * @param id ID of the sent client
   */
  infoClient(id?: number): void {
    // I create an initial object
    this.client = {} as any;

    // I verify the property `id`
    if (id) {
      // build object Client
      this.getOrder(id);
    } else {
      // console.log('no id');
    }
  }

  goBack() {
    this.router.navigateByUrl('administrator/orders/list');
  }

  /**
   * Load the data from orders
   * @param id ID from order
   */
  getOrder(id?: number) {
    this.orderService.listOrders().subscribe(
      (data: IResponse) => {
        data.response.map(order => {
          if (order.id === id) {
            this.updateUserInOrder(order);
          }
        });
      },
      error => {
        this.toastrService.error('Datos no cargados');
      }
    );
  }

  /**
   * adding user object to de object
   * @param order object to update
   */
  updateUserInOrder(order: any) {
    this.clientService.byId(order.client.id).subscribe(
      (data: IResponse) => {
        order['user'] = data.response.user;
        //this.updateMedicalHistoryClient(order);
      },
      error => {
        this.toastrService.error('Datos no cargados');
      }
    )
  }
}
