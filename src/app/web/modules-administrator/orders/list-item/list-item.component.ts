import { Component, OnInit, Input } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {
  @Input() data: any;
  public rowsMessage: string;
  public rows = 0;

  ngOnInit() {
    this.verifyData();
    this.rowsMessage = environment.messages.withoutData;
  }

  verifyData() {
    if (!this.data) {
      this.rows = 1;
    } else if (this.data.length === 0) {
      this.rows = 0;
    } else {
      this.rows = 1;
    }
  }
}
