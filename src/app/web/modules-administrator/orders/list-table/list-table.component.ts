import { Router } from '@angular/router';
import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
//import { PatientInfoComponent } from '../../patient/patient-info/patient-info.component';
import { DeleteOrderComponent } from '../delete-order/delete-order.component';
import { ManageComponent } from '../manage/manage.component';

@Component({
  selector: 'app-list-table',
  templateUrl: './list-table.component.html',
  styleUrls: ['./list-table.component.scss']
})
export class ListTableComponent implements OnInit {
  // Propiedades
  @Input() data: any;
  @Output() isDeletedAQuery = new EventEmitter();
  //@ViewChild(ManageComponent) manageComponent: ManageComponent;
  //@ViewChild(deleteOrderComponent) deleteComponent: DeleteOrderComponent;
  //@ViewChild(PatientInfoComponent) infoComponent: PatientInfoComponent;
  public filterQuery = '';
  public rowsOnPage = 5;
  public sortBy = 'lastName';
  public sortOrder = 'asc';
  public rows = 0;
  public rowsMessage: string;
  public dataByDatable: Array<any> = [];
  private idToDelete;
  public settings = [
    { id: 'client', title: 'Cliente', filter: true },
    { id: 'order', title: 'Orden', filter: true },
    { id: 'budget', title: 'Presupuesto' },
  ];
  public options = [
    { id: 'info', title: 'Ver', idElement: null , idUser: null},
    { id: 'update', title: 'Editar', idElement: null , idUser: null },
    { id: 'delete', title: 'Eliminar', idElement: null , idUser: null}
  ];

  constructor(
    private toastrService: ToastrService,
    private router: Router) {}

  ngOnInit() {
    this.rows = (this.data.length === 0) ? 0 : 1;
    this.rowsMessage = environment.messages.withoutData;
    this.getData();
  }

  /**
   * Call the modal to edit a patient
   * @param id patient to update
   */
  edit(id: number) {
    //this.manageComponent.openModal(id);
  }

  /**
   * Call the modal to edit a patient
   * @param id patient to update
   */
  delete(id: number) {
    //this.deleteComponent.openModal(id);
  }

  /**
   * Call the modal to edit a patient
   * @param id patient to update
   */
  detail(id: number) {
    // this.infoComponent.openModal(id);
  }

  /**
   * Changing the value to look for
   * @param newValue New value
   */
  changeSortBy(newValue: string) {
    this.sortBy = newValue;
  }

  /**
   * Update the list of Patients in the table
   * @param updatePatientList Sent from patientModalComponent
   */
  updateList(updateQueryList) {
    if (updateQueryList) {
      this.isDeletedAQuery.emit(true);
      this.removeDataFromTabla(this.idToDelete);
    }
  }

  /**
   * build object from datatable
   */
  getData() {
    this.data.map(element => {
      this.dataByDatable.push({
        id: element.id,
        idUser: element.patient.id,
        patient: `${element.user.firstName} ${element.user.lastName}`,
        consult: `${element.opinionNose}`,
        budget: '3000$',
        date: `${element.date}`,
      });
    });
  }

  /**
   * get data from output datatable component
   * @param $event event object
   */
  selectedOption($event) {
    switch ($event.id) {
      case 'info':
        this.router.navigateByUrl(`administrator/orders/info/${$event.idElement}`);
        break;
      case 'update':
        this.idToDelete = $event.idElement;
        this.edit($event.idUser);
        break;
      case 'delete':
        this.idToDelete = $event.idElement;
        this.delete($event.idElement);
        break;
      default:
        this.toastrService.error('error', 'error');
        break;
    }
  }

  /**
   * removing element from datatable
   * @param id ID query deleted
   */
  removeDataFromTabla(id: number) {
    this.dataByDatable = [];
    this.data.map((element: any) => {
      if (element.id !== id) {
        this.dataByDatable.push({
          id: element.id,
          patient: `${element.user.firstName} ${element.user.lastName}`,
          consult: `${element.opinionNose}`,
          budget: '3000$',
          date: `${element.date}`,
        });
      }
    });
  }
}
