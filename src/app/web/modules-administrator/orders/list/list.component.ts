import { OrderService } from '../../../../core/services/order.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ManageComponent } from '../manage/manage.component';
import { ClientService } from 'src/app/core/services/client.service';
import { IResponse } from 'src/app/shared/interfaces/response';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  // Propiedades
  //@ViewChild(ManageComponent) manageComponent: ManageComponent;
  public value: boolean;
  public data: any;
  public rows: number;
  public uncheckableRadioModel = '';
  public clients: Array<any> = [];
  public orders: Array<any> = [];

  constructor(
    private orderService: OrderService,
    private clientService: ClientService) {}

  ngOnInit() {
    // Get all patients
    this.getOrders();
  }

  /**
   * get all data from queries
   */
  getOrders() {
    this.orderService.listOrders().subscribe(
      (data: IResponse) => {
        this.orders = data.response;
        this.getPatients();
      },
      err => console.log(err)
    );
  }

  /**
   * get all data from clients services
   */
  getPatients() {
    this.clientService.listClients().subscribe(
      (data: IResponse) => {
        this.clients = data.response;
        this.getData();
      },
      err => console.log(err)
    );
  }

  /**
   * building object from data
   */
  getData() {
    this.orders.map(order => {
      this.clients.map(client => {
        if (order.client.id === client.id) {
          order['user'] = client.user;
        }
      });
    });
    this.data = this.orders;
  }

  changeView(value: any) {
    this.value = value;
  }

  /**
   * Call the modal to create a new order
   */
  new() {
    //this.manageComponent.openModal();
  }

  /**
   * Update the list of Orders in the table
   * @param $event Sent from listTable
   */
  updateOrders(updateOrdersList) {
    if (updateOrdersList) {
      this.getOrders();
    }
  }

  updateList($event) {
    if ($event) {
      this.getOrders();
    }
  }

  backToList() { }
}
