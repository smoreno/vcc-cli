import { ClientService } from './../../../../core/services/client.service';
import { OrderService } from '../../../../core/services/order.service';
import { Component, OnInit, ViewChild, TemplateRef, Output, EventEmitter, ElementRef, ChangeDetectorRef } from '@angular/core';
import { CustomNgbModalConfig } from '../../../../core/configs/ngbmodal/ngbmodal-config';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Order } from 'src/app/shared/interfaces/order';
import { IResponse } from 'src/app/shared/interfaces/response';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.scss']
})
export class ManageComponent implements OnInit {
  // Propiedades
  //@ViewChild('content') contentModal: TemplateRef<any>;
  @Output() updateOrderList = new EventEmitter();
  public title = '';
  public data: any;
  public order;
  public orderFormGroup: FormGroup;
  public submitted: boolean;
  public currentModal: any;
  // public countries: any;
  // public genders: any;
  // public typesDocs: any;
  // public isImageToUploadFront = false;
  // public haveToUpdateFront = false;
  // public editFileFront = true;
  // public removeUploadFront = false;
  // public isImageToUploadBase = false;
  // public haveToUpdateBase = false;
  // public editFileBase = true;
  // public removeUploadBase = false;
  // public isImageToUploadProfile = false;
  // public haveToUpdateProfile = false;
  // public editFileProfile = true;
  // public removeUploadProfile = false;
  // public isImageToUploadCustom = false;
  // public haveToUpdateCustom = false;
  // public editFileCustom = true;
  // public removeUploadCustom = false;
  // public clientPhotos: Object;
  public clientId: number;
  //@ViewChild('fileInput') el: ElementRef;
  frontImageUrl: any = 'assets/images/camera.png';
  baseImageUrl: any = 'assets/images/camera.png';
  profileImageUrl: any = 'assets/images/camera.png';
  customImageUrl: any = 'assets/images/camera.png';
  frontRegistrationForm = this.fb.group({
    photo: [null]
  });
  baseRegistrationForm = this.fb.group({
    photo: [null]
  });
  profileRegistrationForm = this.fb.group({
    photo: [null]
  });
  customRegistrationForm = this.fb.group({
    photo: [null]
  });

  constructor(
    private modalService: CustomNgbModalConfig,
    private clientService: ClientService,
    private orderService: OrderService,
    private cd: ChangeDetectorRef,
    private toastrService: ToastrService,
    private fb: FormBuilder, ) { }

  ngOnInit() {
    this.submitted = false;
    // Get all clients
    this.listClients();

    // I create an initial object
    this.order = {} as Order;

    // Building the form
    this.orderFormGroup = this.fb.group({
      idClient: ['']
    });
  }


  /**
   * Selector to access the fields of the client
   *  object
   */
  get f() {
    return this.orderFormGroup.controls;
  }

  /**
   * Load the data in the case of the update
   * @param id ID of the sent client
   */
  openModal(id?: number): void {
    // I clean the form
    this.orderFormGroup.reset();
    this.submitted = false;
    // I create an initial object
    this.order = {} as Order;
    if (id) {
      this.order.id = id;
      // Get of client
      this.orderService.detail(id).subscribe(
        (data: IResponse) => { // migrar este endpoint al de detail
          this.clientId = data.response[0].client.id;
          //this.getMedicalHistoryClient(data.response[0]);
        },
        err => console.log(err)
      );
    } else {
      this.title = 'Nueva Orden';
      this.order = {};
      //this.currentModal = this.modalService.openXl(this.contentModal);
    }
  }



  // /**
  //  * get detail medical history to update query object
  //  * @param query query object from server
  //  */
  // getMedicalHistoryClient(query) {
  //   this.clientService.medicalHistoryClient(query.patient.id).subscribe(
  //     (data: IResponse) => {
  //       this.orderFormGroup.setValue({
  //         frontPicture: (query.frontPicture) ? query.frontPicture : null,
  //         basePicture: (query.basePicture) ? query.basePicture : null,
  //         profilePicture: (query.profilePicture) ? query.profilePicture : null,
  //         customPicture: (query.customPicture) ? query.customPicture : null,
  //       });

  //       this.frontImageUrl = (query.frontPicture) ? query.frontPicture : 'assets/images/camera.png';
  //       this.baseImageUrl = (query.basePicture) ? query.basePicture : 'assets/images/camera.png';
  //       this.profileImageUrl = (query.profilePicture) ? query.profilePicture : 'assets/images/camera.png';
  //       this.customImageUrl = (query.customPicture) ? query.customPicture : 'assets/images/camera.png';
  //       this.frontRegistrationForm.patchValue({
  //         photo: (query.frontPicture) ? query.frontPicture : [null]
  //       });
  //       this.baseRegistrationForm.patchValue({
  //         photo: (query.baseImageUrl) ? query.frontPicture : [null]
  //       });
  //       this.profileRegistrationForm.patchValue({
  //         photo: (query.profileImageUrl) ? query.frontPicture : [null]
  //       });
  //       this.customRegistrationForm.patchValue({
  //         photo: (query.customImageUrl) ? query.frontPicture : [null]
  //       });

  //       // I open the modal
  //       this.currentModal = this.modalService.openXl(this.contentModal);
  //     },
  //     error => {
  //       console.log(error)
  //     }
  //   )
  // }


  /**
   * Form Validation
   */
  save() {
    this.submitted = true;
    if (this.orderFormGroup.invalid) {
      return;
    }
    // Verifico el método a utilizar
    const method = (this.order.id) ? 'update' : 'create';
    // Cargo los datos del objeto a enviar
    this.order.idPatient = this.orderFormGroup.get('idPatient').value;
    this.order.opinionNose = this.orderFormGroup.get('opinionNose').value;
    this.order.photos =  [
      {
        image: this.frontRegistrationForm.get('photo').value,
        type: 'frontPictures'
      },
      {
        image: this.baseRegistrationForm.get('photo').value,
        type: 'basePictures'
      },
      {
        image: this.profileRegistrationForm.get('photo').value,
        type: 'profilePictures'
      },
      {
        image: this.customRegistrationForm.get('photo').value,
        type: 'customPictures'
      },

    ]
    this.orderService[method](this.order).subscribe(
      (data: IResponse) => {
        if (data.code === 200) {
          this.toastrService.success('Orden creada con éxito');
          this.updateOrderList.emit(true);
          if (method == 'create') {
            //this.savedPhotos(data.response.client.id);
          }
          else {
            //this.savedPhotos(this.clientId);
          }
        } else {
          this.toastrService.error(data.response);
        }
      },
      err => console.log(err)
    );
  }

  /**
   * Close the modal window
   */
  closeModal() {
    this.currentModal.close();
  }


  // /**
  //  * save photos from patients
  //  */
  // savedPhotos(id) {
  //   if (id) {
  //     this.clientPhotos = {
  //       clientId: id,
  //       photos: [
  //         {
  //           image: this.frontRegistrationForm.get('photo').value,
  //           type: 'frontPictures'
  //         },
  //         {
  //           image: this.baseRegistrationForm.get('photo').value,
  //           type: 'basePictures'
  //         },
  //         {
  //           image: this.profileRegistrationForm.get('photo').value,
  //           type: 'profilePictures'
  //         },
  //         {
  //           image: this.customRegistrationForm.get('photo').value,
  //           type: 'customPictures'
  //         },

  //       ]
  //     }

  //     if (this.frontRegistrationForm.get('photo').value || this.baseRegistrationForm.get('photo').value
  //       || this.profileRegistrationForm.get('photo').value || this.customRegistrationForm.get('photo').value) {
  //       this.orderService.updatePhotoQuery(this.clientPhotos).subscribe(
  //         (data: IResponse) => {
  //           this.toastrService.success('Photos cargadas con éxito');
  //           this.currentModal.close();
  //           // I update the records
  //           this.updateOrderList.emit(true);
  //           location.reload();
  //         },
  //         error => {
  //           console.log(error);
  //         }
  //       )
  //     }
  //     else {
  //       this.toastrService.success('Photos cargadas con éxito');
  //       this.currentModal.close();
  //       // I update the records
  //       this.updateOrderList.emit(true);
  //       location.reload();
  //     }


  //   }
  // }


  // /**
  //  * upload image
  //  */

  // uploadFile(event: any) {
  //   const reader = new FileReader(); // HTML5 FileReader API
  //   const file = event.target.files[0];
  //   switch (event.srcElement.id) {
  //     case 'frontUpload':
  //       if (event.target.files && event.target.files[0]) {
  //         reader.readAsDataURL(file);
  //         // When file uploads set it to file formcontrol
  //         reader.onload = () => {
  //           this.frontImageUrl = reader.result;
  //           this.frontRegistrationForm.patchValue({
  //             photo: reader.result,
  //           });
  //         };
  //       }
  //       this.isImageToUploadFront = true;
  //       this.haveToUpdateFront = true;
  //       this.editFileFront = false;
  //       this.removeUploadFront = true;
  //       break;
  //     case 'baseUpload':
  //       if (event.target.files && event.target.files[0]) {
  //         reader.readAsDataURL(file);
  //         // When file uploads set it to file formcontrol
  //         reader.onload = () => {
  //           this.baseImageUrl = reader.result;
  //           this.baseRegistrationForm.patchValue({
  //             photo: reader.result,
  //           });
  //         };
  //       }
  //       this.isImageToUploadBase = true;
  //       this.haveToUpdateBase = true;
  //       this.editFileBase = false;
  //       this.removeUploadBase = true;
  //       break;
  //     case 'profileUpload':
  //       if (event.target.files && event.target.files[0]) {
  //         reader.readAsDataURL(file);
  //         // When file uploads set it to file formcontrol
  //         reader.onload = () => {
  //           this.profileImageUrl = reader.result;
  //           this.profileRegistrationForm.patchValue({
  //             photo: reader.result,
  //           });
  //         };
  //       }
  //       this.isImageToUploadProfile = true;
  //       this.haveToUpdateProfile = true;
  //       this.editFileProfile = false;
  //       this.removeUploadProfile = true;
  //       break;
  //     case 'customUpload':
  //       if (event.target.files && event.target.files[0]) {
  //         reader.readAsDataURL(file);
  //         // When file uploads set it to file formcontrol
  //         reader.onload = () => {
  //           this.customImageUrl = reader.result;
  //           this.customRegistrationForm.patchValue({
  //             photo: reader.result,
  //           });
  //         };
  //       }
  //       this.isImageToUploadCustom = true;
  //       this.haveToUpdateCustom = true;
  //       this.editFileCustom = false;
  //       this.removeUploadCustom = true;
  //       break;

  //     default:
  //       break;
  //   }

  //   // ChangeDetectorRef since file is loading outside the zone
  //   this.cd.markForCheck();

  // }


  changeImage(event, imageUrl, form) {
    const reader = new FileReader(); // HTML5 FileReader API
    const file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      reader.readAsDataURL(file);

      // When file uploads set it to file formcontrol
      reader.onload = () => {
        imageUrl = reader.result;
        form.patchValue({
          photo: reader.result,
        });

      };
      // ChangeDetectorRef since file is loading outside the zone
      this.cd.markForCheck();
    }
  }


  // removeUploadedFile(component) {
  //   switch (component) {
  //     case 'frontUpload':
  //       this.frontImageUrl = 'assets/images/camera.png';
  //       this.editFileFront = true;
  //       this.removeUploadFront = false;
  //       this.frontRegistrationForm.patchValue({
  //         photo: [null]
  //       });
  //       this.isImageToUploadFront = false;
  //       break;
  //     case 'baseUpload':
  //       this.frontImageUrl = 'assets/images/camera.png';
  //       this.editFileBase = true;
  //       this.removeUploadBase = false;
  //       this.baseRegistrationForm.patchValue({
  //         photo: [null]
  //       });
  //       this.isImageToUploadBase = false;
  //       break;
  //     case 'profileUpload':
  //       this.frontImageUrl = 'assets/images/camera.png';
  //       this.editFileProfile = true;
  //       this.removeUploadProfile = false;
  //       this.profileRegistrationForm.patchValue({
  //         photo: [null]
  //       });
  //       this.isImageToUploadProfile = false;
  //       break;
  //     case 'customUpload':
  //       this.frontImageUrl = 'assets/images/camera.png';
  //       this.editFileCustom = true;
  //       this.removeUploadCustom = false;
  //       this.customRegistrationForm.patchValue({
  //         photo: [null]
  //       });
  //       this.isImageToUploadCustom = false;
  //       break;

  //     default:
  //       break;
  //   }
  // }

  listClients() {
    this.clientService.listClients().subscribe(
      (data: IResponse) => {
        this.data = data.response;
      },
      err => console.log(err)
    );
  }
}
