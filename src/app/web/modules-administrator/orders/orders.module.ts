import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdersRoutingModule } from './orders.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

// Components
import { DeleteOrderComponent } from './delete-order/delete-order.component';
import { DetailOrderComponent } from './detail-order/detail-order.component';
import { InfoComponent } from './info/info.component';
import { ListComponent } from './list/list.component';
import { ListItemComponent } from './list-item/list-item.component';
import { ListTableComponent } from './list-table/list-table.component';
import { ManageComponent } from './manage/manage.component';
import { DiagnosticsComponent } from './diagnostics/diagnostics.component';

// Const
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  declarations: [
    DeleteOrderComponent,
    DetailOrderComponent,
    InfoComponent,
    ListComponent,
    ListItemComponent,
    ListTableComponent,
    ManageComponent,
    DiagnosticsComponent,
  ],
  imports: [
    CommonModule,
    OrdersRoutingModule,
    SharedModule,
    PerfectScrollbarModule,
  ],
  providers: [{
    provide: PERFECT_SCROLLBAR_CONFIG,
    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
  }],
})
export class QueriesModule {}
