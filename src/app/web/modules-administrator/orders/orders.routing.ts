import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components
import { ListComponent } from './list/list.component';
import { ManageComponent } from './manage/manage.component';
import { InfoComponent } from './info/info.component';

const routes: Routes = [
  { path: '', component: ListComponent },
  { path: 'new', component: ManageComponent },
  { path: 'edit/:id', component: ManageComponent },
  { path: 'info/:id', component: InfoComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule {}
