import { SummaryService } from './../../../../core/services/summary.service';
import { Router } from '@angular/router';
import { IResponse } from './../../../../shared/interfaces/response';
import { Component, OnInit } from '@angular/core';
import { InfoCardOptions } from 'src/app/shared/interfaces/info-card-options';
import { DatatableSettings } from 'src/app/shared/interfaces/datatable-settings';
import { DatatableOptions } from 'src/app/shared/interfaces/datatable-options';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
  // Properties
  public date: any;
  public rangeDate: any;
  public optionsNewClientCard: InfoCardOptions;
  public optionsOrderCard: InfoCardOptions;
  public optionsScheduleCard: InfoCardOptions;
  public dataGraphics: any;
  public settings: DatatableSettings[] = [
    { id: 'name', title: 'Nombres', filter: true },
    { id: 'lastName', title: 'Apellidos', filter: true },
    { id: 'email', title: 'Correo Electrónico', filter: true },
    { id: 'phone', title: 'Número telefónico', filter: false },
    { id: 'date', title: 'Fecha', filter: false },
  ];
  public options: DatatableOptions[] = [{ id: 'info', title: 'Ver', idElement: null }];
  public data: Array<any> = [];

  constructor(
    private summaryService: SummaryService,
    private router: Router) {}

  ngOnInit() {
    this.getDetailSummary(0);
  }

  /**
   * build object from datatable component
   * @param response response data from server
   */
  getDataClientTable(response: Array<any>) {
    this.data = [];
    response.map((element: any) => {
      this.data.push({
        id: element.id,
        name: element.firstName,
        lastName: element.lastName,
        email: element.email,
        phone: element.phone,
        date: element.createdAt,
      });
    });
  }

  /**
   * redirect to detail client
   * @param id id client
   */
  detail(id: number) {
    this.router.navigateByUrl(`/administrator/client/info/${id}`);
  }

  /**
   * get data from output datatable component
   * @param $event event object
   */
  selectedOption($event: any) {
    switch ($event.id) {
      case 'info': this.detail($event.idElement); break;
    }
  }

  /**
   * get detail from range
   * @param $event range id
   */
  changeRange($event: any) {
    this.getDetailSummary(+$event.target.value);
  }

  /**
   * Get detail from range in backend
   * @param $option range id
   */
  getDetailSummary(option: number) {
    const since = this.formatDate();
    const date = new Date();
    let until: any;
    switch (option) {
      case 0:
        // 30 days
        date.setDate(date.getDate() - 30);
        until = date.toISOString().split('T')[0];
        this.getData(until, since);
        break;
      case 1:
        // 3 month
        date.setMonth(date.getMonth() - 3);
        until = date.toISOString().split('T')[0];
        this.getData(until, since);
        break;
      case 2:
        // a year
        date.setDate(date.getDate() - 365);
        until = date.toISOString().split('T')[0];
        this.getData(until, since);
        break;
      default:
        date.setDate(date.getDate() - 30);
        until = date.toISOString().split('T')[0];
        this.getData(until, since);
        break;
    }
  }

  /**
   * Get data from backend
   */
  getData(until: string, since: string) {
    this.summaryService.get(until, since).subscribe(
      (data: IResponse) => {
        this.getDataClientTable(data.response.recentOrders);
        this.setOptionsOrderCard(data.response);
        this.setOptionsNewClientCard(data.response);
        this.setOptionsScheduleCard(data.response);
        this.setDataGraphics(data.response);
      },
      error => console.log(error)
    );
  }

  /**
   * Set graphics data
   */
  setDataGraphics(data: any) {
    this.dataGraphics = data.graphicMedicalQueries;
  }

  /**
   * Set optionsScheduleCard
   */
  setOptionsScheduleCard(data: any) {
    this.optionsScheduleCard = {
      icon: 'fas fa-calendar',
      title: data.countAppointments,
      subtitle: 'Programadas Hoy',
      actionTitle: 'Abrir Agenda',
      startColor: '#3ba7ff',
      endColor: '#b791ff',
      backgroundWhite: false,
      path: '../calendar'
    };
  }

  /**
   * Set optionsOrderCard
   */
  setOptionsOrderCard(data: any) {
    this.optionsOrderCard = {
      icon: 'fas fa-address-card',
      title: data.countMedicalQueries,
      subtitle: 'Consultas Recientes',
      actionTitle: 'Ir a Consultas',
      startColor: '#ffa9293d',
      endColor: '#ffa929',
      backgroundWhite: true,
      path: '../queries'
    };
  }

  /**
   * Set optionsNewClientCard
   */
  setOptionsNewClientCard(data: any) {
    this.optionsNewClientCard = {
      icon: 'fas fa-user',
      title: data.countPatients,
      subtitle: 'Nuevos clientes',
      actionTitle: 'Ir a clientes',
      startColor: '#419bf97d',
      endColor: '#3b86ff',
      backgroundWhite: true,
      path: '../client'
    };
  }

  /**
   * Get today data with format yyyy-mm-dd
   */
  formatDate() {
    const d = new Date();
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    const year = d.getFullYear();
    if (month.length < 2) { month = '0' + month; }
    if (day.length < 2) { day = '0' + day; }
    return [year, month, day].join('-');
  }

  goToOrders() {
    this.router.navigateByUrl('/administrator/orders/list');
  }
}
