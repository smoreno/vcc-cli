import { Component, OnInit, Input } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-summary-graphics',
  templateUrl: './summary-graphics.component.html',
  styleUrls: ['./summary-graphics.component.scss']
})
export class SummaryGraphicsComponent implements OnInit {
  // Properties
  @Input() data: any;
  LineChart = [];

  ngOnInit() {
    const canvas: any = document.getElementById('lineChart');
    const ctx = canvas.getContext('2d');
    const gradient = ctx.createLinearGradient(0, 0, 0, 450);
    gradient.addColorStop(0, 'rgba(202, 232,248, 1)');
    gradient.addColorStop(0.5, 'rgba(202, 232,248, 0.5)');
    gradient.addColorStop(1, 'rgba(221, 213, 250, 1)');

    this.LineChart = new Chart('lineChart', {
      type: 'line',
      data: {
        labels: this.data.months,
        datasets: [{
          label: 'En línea',
          data: this.data.information,
          borderColor: false,
          fill: true,
          backgroundColor: gradient,
          pointBackgroundColor: 'transparent',
        }]
      },
      options: {
        legend: { position: 'bottom' },
        title: { text: 'Consultas en Línea', display: true },
        scales: {
          yAxes: [{
            ticks: { beginAtZero: true }
          }]
        }
      }
    });
  }
}
