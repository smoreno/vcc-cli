import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SummaryRoutingModule } from './summary.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { IndexComponent } from './index/index.component';
import { SummaryGraphicsComponent } from './summary-graphics/summary-graphics.component';

@NgModule({
  declarations: [
    IndexComponent,
    SummaryGraphicsComponent,
  ],
  imports: [
    CommonModule,
    SummaryRoutingModule,
    SharedModule,
  ]
})
export class SummaryModule {}
