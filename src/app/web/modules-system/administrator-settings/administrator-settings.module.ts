import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { AdministratorSettingsRoutingModule } from './administrator-settings.routing';

// Components
import { IndexComponent } from './index/index.component';
import { ProfileFormComponent } from './profile-form/profile-form.component';

@NgModule({
  declarations: [
    IndexComponent,
    ProfileFormComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    AdministratorSettingsRoutingModule,
  ]
})
export class AdministratorSettingsModule {}
