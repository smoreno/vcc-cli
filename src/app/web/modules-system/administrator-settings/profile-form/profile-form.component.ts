import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { IResponse } from 'src/app/shared/interfaces/response';
import { ToastrService } from 'ngx-toastr';
import { AdministratorService } from 'src/app/core/services/administrator.service';
import { Administrator } from 'src/app/shared/interfaces/administrator';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss']
})
export class ProfileFormComponent implements OnInit {
  public profileFormGroup: FormGroup;
  public administrator: Administrator;
  public submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private administratorService: AdministratorService,
    private lsService: LocalStorageService,
    private toastrService: ToastrService) {}

  ngOnInit() {
    // Build form
    this.profileFormGroup = this.formBuilder.group({
      firstName: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20)
      ]],
      lastName: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20)
      ]],
      email: ['', [
        Validators.required,
        Validators.pattern(/\b\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+\b/)
      ]],
    });

    // Get id
    const administratorId = parseInt(this.lsService.getValue('id'));

    // Get of settings
    this.administratorService.allSetting(administratorId).subscribe(
      (data: IResponse) => {
        this.profileFormGroup.setValue({
          firstName: data.response.firstName,
          lastName: data.response.lastName,
          email: data.response.email,
        });
      },
      err => console.log(err)
    );
  }

  get f() { return this.profileFormGroup.controls; }

  updateProfile() {
    this.submitted = true;
    if (this.profileFormGroup.invalid) {
      return;
    }

    // Get id
    const administratorId = parseInt(this.lsService.getValue('id'));

    // Set new doctor
    this.administrator = {} as Administrator;
    this.administrator.id = administratorId;
    this.administrator.firstName = this.profileFormGroup.get('firstName').value;
    this.administrator.lastName = this.profileFormGroup.get('lastName').value;
    this.administrator.email = this.profileFormGroup.get('email').value;

    // Updated profile doctor
    this.administratorService.update(this.administrator).subscribe(
      (data: IResponse) => {
        this.lsService.setValue('firstName', this.administrator.firstName);
        this.lsService.setValue('lastName', this.administrator.lastName);
        this.lsService.setValue('name', this.administrator.firstName + ' ' + this.administrator.lastName);
        this.lsService.setValue('email', this.administrator.email);
        this.toastrService.success('Perfil actualizado con éxito');
      },
      err => console.log(err)
    );
  }
}
