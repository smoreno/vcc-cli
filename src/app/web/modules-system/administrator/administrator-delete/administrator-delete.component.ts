import { Component, Output, ViewChild, EventEmitter, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

// Services
import { AdministratorService } from 'src/app/core/services/administrator.service';
import { CustomNgbModalConfig } from '../../../../core/configs/ngbmodal/ngbmodal-config';

// Interfaces
import { IResponse } from 'src/app/shared/interfaces/response';

@Component({
  selector: 'app-administrator-delete',
  templateUrl: './administrator-delete.component.html',
  styleUrls: ['./administrator-delete.component.scss']
})
export class AdministratorDeleteComponent {
  // Propiedades
  //@ViewChild('content') contentModal: TemplateRef<any>;
  @Output() updateAdministratorList = new EventEmitter();
  public currentModal: any;
  public administratorId: number;

  /**
   * Class constructor
   * @param AdministratorService Service of administrator
   * @param modalService Modal service
   */
  constructor(
    private modalService: CustomNgbModalConfig,
    private administratorService: AdministratorService,
    private toastrService: ToastrService) {}

  openModal(id: number): void  {
    if (id) {
      this.administratorId = id;
      //this.currentModal = this.modalService.openSm(this.contentModal);
    } else {
      this.toastrService.error('Ocurrio un error inesperado, por favor intente nuevamente');
    }
  }

  remove() {
    this.administratorService.removeAdministrator(this.administratorId ).subscribe(
      (data: IResponse) => {
        if (data.response.affected === 1) {
          this.toastrService.success('Administrador eliminado');
          this.updateAdministratorList.emit(true);
          this.currentModal.close();
        } else {
          this.toastrService.error('Ocurrio un error inesperado, por favor intente nuevamente');
        }
      },
      err => console.log(err)
    );
  }

  /**
   * Close the modal window
   */
  closeModal() {
    this.currentModal.close();
  }
}
