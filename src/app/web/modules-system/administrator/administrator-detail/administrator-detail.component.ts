import { Component, ViewChild, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

// Interfaces
import { IResponse } from 'src/app/shared/interfaces/response';
import { Administrator } from 'src/app/shared/interfaces/administrator';

// Services
import { AdministratorService } from 'src/app/core/services/administrator.service';
import { CustomNgbModalConfig } from '../../../../core/configs/ngbmodal/ngbmodal-config';

@Component({
  selector: 'app-administrator-detail',
  templateUrl: './administrator-detail.component.html',
  styleUrls: ['./administrator-detail.component.scss']
})
export class AdministratorDetailComponent {
  // Propiedades
  //@ViewChild('content') contentModal: TemplateRef<any>;
  public administrator: Administrator[];
  public currentModal: any;
  public image: string;

  constructor(
    private modalService: CustomNgbModalConfig,
    private administratorService: AdministratorService,
    private toastrService: ToastrService) {}

  /**
   * Open the modal detail administrator
   */
  detail(id: number) {
    this.administratorService.byId(id).subscribe(
      (data: IResponse) => {
        this.toastrService.success('Datos cargados con éxito');
        this.administrator = data.response;
        this.image = (data.response.photo) ? data.response.photo : 'assets/images/avatar.png';
        //this.currentModal = this.modalService.openLg(this.contentModal);
      },
    );
  }

  /**
   * Close the modal window
   */
  closeModal() {
    this.currentModal.close();
  }
}
