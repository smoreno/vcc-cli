import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

// Components
import { AdministratorManageComponent } from 'src/app/web/modules-system/administrator/administrator-manage/administrator-manage.component';
import { AdministratorDeleteComponent } from 'src/app/web/modules-system/administrator/administrator-delete/administrator-delete.component';
import { AdministratorDetailComponent } from 'src/app/web/modules-system/administrator/administrator-detail/administrator-detail.component';
import { UploadPhotoComponent } from 'src/app/web/modules-system/administrator/upload-photo/upload-photo.component';
import { ChangePasswordComponent } from 'src/app/web/modules-system/administrator/change-password/change-password.component';

// Interfaces
import { IResponse } from '../../../../shared/interfaces/response';
import { Administrator } from '../../../../shared/interfaces/administrator';
import { DatatableSettings } from 'src/app/shared/interfaces/datatable-settings';
import { DatatableOptions } from 'src/app/shared/interfaces/datatable-options';

// Services
import { AdministratorService } from '../../../../core/services/administrator.service';

@Component({
  selector: 'app-administrator-list',
  templateUrl: './administrator-list.component.html',
  styleUrls: ['./administrator-list.component.scss'],
})
export class AdministratorListComponent implements OnInit {
  // Properties
  /*@ViewChild(AdministratorManageComponent) manageComponent: AdministratorManageComponent;
  @ViewChild(AdministratorDeleteComponent) administratorDeleteComponent: AdministratorDeleteComponent;
  @ViewChild(AdministratorDetailComponent) administratorDetailComponent: AdministratorDetailComponent;
  @ViewChild(UploadPhotoComponent) uploadPhotoComponent: UploadPhotoComponent;
  @ViewChild(ChangePasswordComponent) changePasswordComponent: ChangePasswordComponent;*/
  public data: any;
  public administrators: Administrator[];
  public settings: DatatableSettings[] = [
    { id: 'firstName', title: 'Nombres', filter: true },
    { id: 'lastName', title: 'Apellidos', filter: true },
    { id: 'email', title: 'Correo Electrónico', filter: true }
  ];
  public options: DatatableOptions[] = [
    { id: 'info', title: 'Ver', idElement: null },
    { id: 'update', title: 'Editar', idElement: null },
    { id: 'updatePhoto', title: 'Gestionar Foto', idElement: null },
    { id: 'delete', title: 'Eliminar', idElement: null }
  ];

  /**
   * Class constructor
   * @param administratorService Service of administrator
   */
  constructor(
    private administratorService: AdministratorService,
    private toastrService: ToastrService) {}

  ngOnInit() {
    this.listAdministrators();
  }

  listAdministrators() {
    this.administratorService.listAdministrators().subscribe(
      (data: IResponse) => {
        this.toastrService.success('Datos cargados con éxito');
        this.data = data.response;
      },
      err => console.log(err)
    );
  }

  /**
   * Call the modal to create a new administrators
   */
  new() {
    //this.manageComponent.openModal();
  }

  /**
   * Call the modal to edit a administrators
   * @param id administrators to update
   */
  edit(id: number) {
    //this.manageComponent.openModal(id);
  }

  /**
   * Call the modal to edit a administrators
   * @param id administrators to update
   */
  delete(id: number) {
    //this.administratorDeleteComponent.openModal(id);
  }

  /**
   * Call the modal to edit a administrators
   * @param id administrators to update
   */
  detail(id: number) {
    //this.administratorDetailComponent.detail(id);
  }

  /**
   * Call the modal to manage photo
   * @param id Doctor to manage photo
   */
  uploadPhoto(id: number) {
    //this.uploadPhotoComponent.openModal(id);
  }

  /**
   * Call the modal to manage photo
   * @param id Doctor to manage photo
   */
  changePass() {
    //this.changePasswordComponent.openModal();
  }

  /**
   * Update the list of Administrators in the table
   * @param updateAdministratorList Sent from administratorModalComponent
   */
  updateList(updateAdministratorList: any) {
    if (updateAdministratorList) {
      this.listAdministrators();
    }
  }

  /**
   * get data from output datatable component
   * @param $event event object
   */
  selectedOption($event: any) {
    switch ($event.id) {
      case 'info':
        this.detail($event.idElement);
        break;
      case 'update':
        this.edit($event.idElement);
        break;
      case 'updatePhoto':
        this.uploadPhoto($event.idElement);
        break;
      case 'delete':
        this.delete($event.idElement);
        break;
      default:
        this.toastrService.error('error', 'error');
        break;
    }
  }
}
