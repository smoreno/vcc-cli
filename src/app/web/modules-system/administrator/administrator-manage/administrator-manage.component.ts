import { Component, OnInit, Output, ViewChild, EventEmitter, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IResponse } from '../../../../shared/interfaces/response';
import { MustMatch } from '../../../../shared/helpers/must-match.validator';

// Interfaces
import { Administrator } from '../../../../shared/interfaces/administrator';

// Services
import { AdministratorService } from '../../../../core/services/administrator.service';
import { CustomNgbModalConfig } from '../../../../core/configs/ngbmodal/ngbmodal-config';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-administrator-manage',
  templateUrl: './administrator-manage.component.html',
  styleUrls: ['./administrator-manage.component.scss']
})
export class AdministratorManageComponent implements OnInit {
  // Propiedades
  //@ViewChild('content') contentModal: TemplateRef<any>;
  @Output() updateAdministratorList = new EventEmitter();
  public title = '';
  public administrator: Administrator;
  public submitted: boolean;
  public administratorFormGroup: FormGroup;
  public currentAdministrator: Administrator;
  public currentModal: any;
  public isEdit = false;

  /**
   * Class constructor
   * @param modalService Modal service
   * @param fb FormGroup builder
   * @param administratorService Service of the consultations of an Administrator
   * @param toastr Message service
   */
  constructor(
    private fb: FormBuilder,
    private administratorService: AdministratorService,
    private toastrService: ToastrService,
    private modalService: CustomNgbModalConfig) {}

  /**
   * Building the FormBuilder
   */
  ngOnInit() {
    this.submitted = false;

    // Building the form
    this.administratorFormGroup = this.fb.group({
      firstName: ['', [ Validators.required ] ],
      lastName: ['', [ Validators.required ] ],
      email: ['', [
        Validators.required,
        Validators.pattern(/\b\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+\b/) ] ],
      photo: [''],
      password: ['',
      [ Validators.required,
        Validators.minLength(6),
        Validators.maxLength(10),
       ] ],
      confirmPassword: ['',
      [ Validators.required,
        Validators.minLength(6),
        Validators.maxLength(10),
      ] ],
    }, {
      validator: MustMatch('password', 'confirmPassword'),
    });
  }

  /**
   * Selector to access the fields of the administrator object
   */
  get f() {
    return this.administratorFormGroup.controls;
  }

  /**
   * Load the data in the administrator of the update
   * @param id ID of the sent administrator
   */
  openModal(id?: number): void {
    // I clean the form
    this.administratorFormGroup.reset();
    this.submitted = false;

    // I create an initial object
    this.administrator = { } as Administrator;

    // I verify the property `id`
    if (id) {
      this.title = 'Editar Administrador';
      // Get of administrator
      this.administratorService.byId(id).subscribe(
        (data: IResponse) => {
          this.administratorFormGroup.setValue({
            firstName: data.response.firstName,
            lastName: data.response.lastName,
            email: data.response.email,
            password: data.response.password,
            confirmPassword: data.response.password,
            photo: null,
          });
          this.isEdit = true;
          this.disablePass(id);
          // I open the modal
          //this.currentModal = this.modalService.openLg(this.contentModal);
        },
          err => console.log(err)
        );
    } else {
      this.title = 'Nuevo Administrador';
      this.isEdit = false;
      this.disablePass();
      //this.currentModal = this.modalService.openLg(this.contentModal);
    }
  }

  disablePass(id?: number): void {
    this.administrator.id = id;
    if (this.isEdit) {
      this.administratorFormGroup.get('password').setValidators(null);
      this.administratorFormGroup.get('confirmPassword').setValidators(null);
      this.isEdit = true;
    } else if (!this.isEdit) {
      this.administratorFormGroup.get('password').setValidators(Validators.required);
      this.administratorFormGroup.get('confirmPassword').setValidators(Validators.required);
      this.isEdit = false;
    }
  }

  /**
   * Form Validation
   */
  save() {
    this.submitted = true;
    if (this.administratorFormGroup.invalid) {
     return;
    }

    // Verifico el método a utilizar
    const method = (this.administrator.id) ? 'update' : 'create';

    // Cargo los datos del objeto a enviar
    this.administrator.firstName = this.administratorFormGroup.get('firstName').value;
    this.administrator.lastName = this.administratorFormGroup.get('lastName').value;
    this.administrator.email = this.administratorFormGroup.get('email').value;
    this.administrator.photo = this.administratorFormGroup.get('photo').value;
    this.administrator.password = this.administratorFormGroup.get('password').value;

    this.administratorService[method](this.administrator).subscribe(
      (data: IResponse) => {
        if (data.code === 200) {
          this.toastrService.success('Administrador creado con éxito');
          this.currentModal.close();
          this.updateAdministratorList.emit(true);
        } else {
          this.toastrService.error(data.response);
        }
      },
      err => console.log(err)
    );
  }

  /**
   * Close the modal window
   */
  closeModal() {
    this.currentModal.close();
  }
}
