import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministratorRoutingModule } from './administrator.routing';
import { SharedModule } from 'src/app/shared/shared.module';

// Components
import { AdministratorListComponent } from './administrator-list/administrator-list.component';
import { AdministratorManageComponent } from './administrator-manage/administrator-manage.component';
import { AdministratorDetailComponent } from './administrator-detail/administrator-detail.component';
import { AdministratorDeleteComponent } from './administrator-delete/administrator-delete.component';
import { UploadPhotoComponent } from './upload-photo/upload-photo.component';
import { ChangePasswordComponent } from './change-password/change-password.component';

@NgModule({
  declarations: [
    AdministratorListComponent,
    AdministratorManageComponent,
    AdministratorDetailComponent,
    AdministratorDeleteComponent,
    UploadPhotoComponent,
    ChangePasswordComponent,
  ],
  imports: [
    CommonModule,
    AdministratorRoutingModule,
    SharedModule,
  ],
})
export class AdministratorModule {}
