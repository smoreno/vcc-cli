import { Component, ViewChild, TemplateRef } from '@angular/core';

// Services
import { CustomNgbModalConfig } from '../../../../core/configs/ngbmodal/ngbmodal-config';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent {
  // Propiedades
  //@ViewChild('content') contentModal: TemplateRef<any>;
  public currentModal: any;

  constructor(private modalService: CustomNgbModalConfig) {}

  /**
   * Open the modal upload photo
   */
  openModal() {
    //this.currentModal = this.modalService.openSm(this.contentModal);
  }

  /**
   * Close the modal window
   */
  closeModal() {
    this.currentModal.close();
  }
}
