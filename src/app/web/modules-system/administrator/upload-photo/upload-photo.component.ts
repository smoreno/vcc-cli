import { Component, ViewChild, TemplateRef } from '@angular/core';

// Services
import { CustomNgbModalConfig } from '../../../../core/configs/ngbmodal/ngbmodal-config';

@Component({
  selector: 'app-upload-photo',
  templateUrl: './upload-photo.component.html',
  styleUrls: ['./upload-photo.component.scss']
})
export class UploadPhotoComponent {
  /*
  *for update a external avatar or logo , different to user logged
  */
  public administratorId: number = null;
  // Propiedades
  //@ViewChild('content') contentModal: TemplateRef<any>;
  public currentModal: any;

  constructor(private modalService: CustomNgbModalConfig) {}

  /**
   * Open the modal upload photo
   */
  openModal(id: number) {
    this.administratorId = id;
    //this.currentModal = this.modalService.openLg(this.contentModal);
  }

  /**
   * Close the modal window
   */
  closeModal() {
    this.currentModal.close();
  }
}
