export const environment = {
  production: true,
  apiRoot: 'https://api',
  messages: {
    success: 'Operación realizada con éxito',
    error: 'Ocurrió un error inesperado, por favor intente nuevamente',
    deleteSucces: 'Se ha eliminado exitosamente este registro',
    withoutData: 'Sin registros para mostrar',
    logout: 'Cierre de Sesión exitoso',
    login: 'Inicio de Sesión exitoso',
    successData: 'Datos cargados exitosamente'
  }
};
